//
// Created by Jamie Briggs on 06/12/2023.
// Copyright (c) 2023 jbriggs.dev All rights reserved.
//

#ifndef ENGINESCHEMATIC_H
#define ENGINESCHEMATIC_H
#include <map>

#include "../core/Vector2D.h"
#include "../helper/SolutionInput.h"

namespace helper {
  class SolutionInput;
}

namespace day03 {

  struct EngineNumberReference {
    int fullNumber;
    core::Vector2D start;
    core::Vector2D end;
  };


  class EngineSchematic {
    int maxY, maxX;
    char** grid;
    vector<std::pair<int, core::Vector2D>> numbers;
    vector<std::pair<char, core::Vector2D>> symbols;
    vector<core::Vector2D> gears;

  public:
    EngineSchematic(const helper::SolutionInput* input);

    char getChar(int x, int y);

    void printNumbers();

    void printSymbols();

    bool isAdjacentToSymbol(int number, core::Vector2D startingPosition);

    bool containsSymbol(core::Vector2D pos);

    int getGearRatio(core::Vector2D pos);

    EngineNumberReference getFullNumber(core::Vector2D pos);

    vector<std::pair<int, core::Vector2D>> getNumbers() {
      return this->numbers;
    }

    vector<core::Vector2D> getGears() {
      return this->gears;
    }
  };
} // day03

#endif //ENGINESCHEMATIC_H
