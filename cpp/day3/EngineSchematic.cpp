//
// Created by Jamie Briggs on 06/12/2023.
// Copyright (c) 2023 jbriggs.dev All rights reserved.
//

#include "EngineSchematic.h"

#include <iostream>
#include <algorithm>
#include <iterator>
#include <set>

#include "..\helper\VectorUtils.h"

namespace day03 {
  vector<char> getAllSymbols(const helper::SolutionInput* input) {
    set<char> allSymbolsSet;

    for (int y = 0; y < input->getTestInput().size(); y++) {
      for (int x = 0; x < input->getTestInput()[0].size(); x++) {
        char character = input->getTestInput()[y][x];
        if (!(character >= '0' && character <= '9') && character != '.') {
          allSymbolsSet.insert(character);
        }
      }
    }

    vector<char> allSymbols(allSymbolsSet.begin(), allSymbolsSet.end());
    return allSymbols;
  }

  EngineSchematic::EngineSchematic(const helper::SolutionInput* input) {
    vector<char> symbolsToCheck = getAllSymbols(input);
    vector<char> noneNumeric(symbolsToCheck);
    noneNumeric.push_back('.');
    this->maxY = input->getTestInput().size();
    this->maxX = input->getTestInput()[0].size();
    grid = new char *[this->maxY];
    for (int ny = 0; ny < this->maxY; ny++) {
      grid[ny] = new char[this->maxX];
      bool isTrackingNumber = false;
      int currentNumber = 0;
      core::Vector2D currentVector;
      for (int nx = 0; nx < this->maxX; nx++) {
        core::Vector2D position = {
          .x = nx,
          .y = ny
        };
        char character = input->getTestInput()[ny][nx];
        grid[ny][nx] = character;
        if (helper::ArrayUtils::isInArray(noneNumeric, character)) {
          if (isTrackingNumber) {
            isTrackingNumber = false;
            this->numbers.emplace_back(currentNumber, currentVector);
            currentNumber = 0;
          }
          if (helper::ArrayUtils::isInArray(symbolsToCheck, character)) {
            this->symbols.emplace_back(character, position);
            if(character == '*') {
              this->gears.push_back(position);
            }
          }
        } else if (character >= '0' && character <= '9') {
          if (isTrackingNumber != true) {
            currentVector = {.x = nx, .y = ny};
            isTrackingNumber = true;
          }
          currentNumber = currentNumber * 10 + character - '0';
        }

        // Handle end of row as above snippet will not add the number after going to the next
        //  line
        if(nx == this->maxX - 1 && isTrackingNumber) {
          isTrackingNumber = false;
          this->numbers.emplace_back(currentNumber, currentVector);
          currentNumber = 0;
        }
      }
    }
  }

  char EngineSchematic::getChar(const int x, const int y) {
    return grid[y][x];
  }

  void EngineSchematic::printNumbers() {
    const auto numbers = this->numbers;
    std::cout << "Total numbers: " << numbers.size() << std::endl;
    for (const auto& pair: numbers) {
      bool isAdjacent = isAdjacentToSymbol(pair.first, pair.second);
      printf("%d\t->\t(%d, %d)\t%d\n", pair.first, pair.second.x, pair.second.y, isAdjacent);
    }
  }

  void EngineSchematic::printSymbols() {
    const auto symbols = this->symbols;
    std::cout << "Total symbols: " << symbols.size() << std::endl;
    for (const auto& pair: symbols) {
      printf("%c -> (%d, %d)\n", pair.first, pair.second.x, pair.second.y);
    }
  }

  bool EngineSchematic::isAdjacentToSymbol(int number, core::Vector2D startingPosition) {
    int totalDigits = to_string(number).length();
    const auto symbols = this->symbols;
    int sy = startingPosition.y;
    int sx = startingPosition.x;
    for (int y = sy - 1; y <= sy + 1; y++) {
      for (int x = sx - 1; x <= sx + totalDigits; x++) {
        if (containsSymbol({.x = x, .y = y})) {
          return true;
        }
      }
    }
    return false;
  }

  bool EngineSchematic::containsSymbol(core::Vector2D pos) {
    for (const auto& symbol: this->symbols) {
      if (symbol.second.x == pos.x && symbol.second.y == pos.y) {
        return true;
      }
    }
    return false;
  }

  int EngineSchematic::getGearRatio(core::Vector2D pos) {
    vector<int> numbersFound;
    for(int y = pos.y - 1; y <= pos.y + 1; y++) {
      for(int x = pos.x - 1; x <= pos.x + 1; x++) {
        char character = this->getChar(x, y);
        if(character >= '0' && character <= '9') {
          auto fullNumber = this->getFullNumber({.x = x, .y = y});
          //  Get full number and progress X past full number
          numbersFound.emplace_back(fullNumber.fullNumber);
          x = fullNumber.end.x;
        }
      }
    }

    if(numbersFound.size() == 2) {
      return numbersFound[0] * numbersFound[1];
    }
    return 0;
  }

  EngineNumberReference EngineSchematic::getFullNumber(core::Vector2D pos) {
    // Find start of number;
    int currentX = pos.x - 1;
    core::Vector2D start{}, end{};
    while(true) {
      if(currentX < 0) {
        start = {.x = 0, .y = pos.y};
        break;
      }
      char character = this->getChar(currentX, pos.y);
      if(character >= '0' && character <= '9') {
        currentX--;
      }else {
        start = {.x = currentX + 1, .y = pos.y};
        break;
      }
    }

    // Find end of number;
    currentX = pos.x + 1;
    while(true) {
      if(currentX > this->maxX) {
        end = {.x = maxX, .y = pos.y};
        break;
      }
      const char character = this->getChar(currentX, pos.y);
      if(character >= '0' && character <= '9') {
        currentX++;
      }else {
        end = {.x = currentX, .y = pos.y};
        break;
      }
    }

    // Get full number
    int fullNumber = 0;
    for(int x = start.x; x < end.x; x++) {
      fullNumber = fullNumber * 10 + (getChar(x, pos.y) - '0');
    }

    const EngineNumberReference reference = {
    .fullNumber = fullNumber,
    .start = start,
    .end = end
    };

    return reference;
  }
} // day03
