//
// Created by Jamie Briggs on 06/12/2023.
// Copyright (c) 2023 jbriggs.dev All rights reserved.
//

#include "Day03.h"

#include <iostream>

#include "EngineSchematic.h"

using namespace std;
using namespace helper;

string solutions::Day03::solvePartOne(const SolutionInput* input) {
  auto* engine_schematic = new day03::EngineSchematic(input);

  const auto numbers = engine_schematic->getNumbers();
  int total = 0;
  for (const auto& number: numbers) {
    if(engine_schematic->isAdjacentToSymbol(number.first, number.second)) {
      total += number.first;
    }
  }
  return to_string(total);
}


string solutions::Day03::solvePartTwo(const SolutionInput* input) {
  auto* engine_schematic = new day03::EngineSchematic(input);

  const auto gears = engine_schematic->getGears();
  int total = 0;
  for (const auto& gear: gears) {
    const int gearRatio = engine_schematic->getGearRatio(gear);
    total += gearRatio;
  }
  return to_string(total);
}
