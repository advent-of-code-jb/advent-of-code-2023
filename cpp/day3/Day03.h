//
// Created by Jamie Briggs on 06/12/2023.
// Copyright (c) 2023 jbriggs.dev All rights reserved.
//

#ifndef DAY03_H
#define DAY03_H
#include "../Day.h"


namespace solutions {


class Day03 final : public Day {
  std::string solvePartOne(const helper::SolutionInput* input) override;
  std::string solvePartTwo(const helper::SolutionInput* input) override;
};

}


#endif //DAY03_H
