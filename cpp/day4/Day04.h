//
// Created by Jamie Briggs on 05/12/2023.
// Copyright (c) 2023 jbriggs.dev All rights reserved.
//

#ifndef DAY_04_H
#define DAY_04_H

#include <string>

#include "../Day.h"
#include "../helper/SolutionInput.h"

using namespace helper;

namespace solutions {
class Day04 final : public Day {
public:
  std::string solvePartOne(const helper::SolutionInput *input) override;
  std::string solvePartTwo(const helper::SolutionInput *input) override;
};
} // namespace solutions

#endif // DAY_04_H
