//
// Created by Jamie Briggs on 10/12/2023.
// Copyright (c) 2023 jbriggs.dev All rights reserved.
//

#include "ScratchCard.h"

#include "../helper/StringUtils.h"

#include <iostream>
#include <regex>

using namespace std;

namespace day04 {
ScratchCard::ScratchCard(const std::string &input) {
  const regex carRegex("Card\\s+([0-9])+: ([0-9 ]+) \\| ([0-9 ]+)");
  smatch m;
  regex_search(input, m, carRegex);

  this->cardNumber = stoi(m[1]);
  const vector<string> rawWinningNumbers =
      helper::StringUtils::splitString(m[2], " ");
  const vector<string> rawCardNumbers =
      helper::StringUtils::splitString(m[3], " ");
  for (const auto &number : rawWinningNumbers) {
    if (!number.empty()) {
      this->winingNumbers.emplace_back(stoi(number));
    }
  }
  for (const auto &number : rawCardNumbers) {
    if (!number.empty()) {
      this->cardNumbers.emplace_back(stoi(number));
    }
  }
}
ScratchCard::ScratchCard(ScratchCard *card) {
  this->cardNumber = card->cardNumber;
  this->winingNumbers = vector<int>(winingNumbers);
  this->cardNumbers = vector<int>(cardNumbers);
}

int ScratchCard::pointsWorth() {
  int totalWinningNumbers = this->totalMatches();

  if (totalWinningNumbers == 0)
    return 0;

  int totalPoints = 1;
  // Start on 2 due to points starting on 1 for first match
  for (int i = 2; i <= totalWinningNumbers; i++) {
    totalPoints *= 2;
  }

  return totalPoints;
}
int ScratchCard::totalMatches() {
  int totalWinningNumbers = 0;
  for (const auto &winningNumber : this->winingNumbers) {
    for (const auto &number : this->cardNumbers) {
      if (winningNumber == number) {
        totalWinningNumbers++;
      }
    }
  }

  return totalWinningNumbers;

}
} // namespace day04