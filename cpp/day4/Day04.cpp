//
// Created by Jamie Briggs on 05/12/2023.
// Copyright (c) 2023 jbriggs.dev All rights reserved.
//

#include "Day04.h"

#include "ScratchCard.h"

#include <iostream>
#include <numeric>
#include <vector>

#include "../helper/NumberUtils.h"
#include "../helper/SolutionInput.h"

using namespace std;

namespace solutions {
string Day04::solvePartOne(const helper::SolutionInput *input) {
  vector<day04::ScratchCard*> cards;
  for(int i = 0; i < input->getTotalRowsInInput(); i++) {
    string basic_string = input->getTestInput().at(i);
    cards.emplace_back(new day04::ScratchCard(basic_string));
  }

  int totalPoints = 0;
  for(const auto card : cards) {
    totalPoints += card->pointsWorth();
  }
  return to_string(totalPoints);
}

string Day04::solvePartTwo(const helper::SolutionInput *input) {
  vector<day04::ScratchCard*> cards;
  for(int i = 0; i < input->getTotalRowsInInput(); i++) {
    string basic_string = input->getTestInput().at(i);
    cards.emplace_back(new day04::ScratchCard(basic_string));
  }

  for(int i = 0; i < cards.size(); i++) {
    int totalMatches = cards[i]->totalMatches();
    vector<day04::ScratchCard*> cardsToAdd;
    // Get all cards first to duplicate
    for(int mi = i + 1; mi <= i + totalMatches; mi++) {
      cardsToAdd.emplace_back(new day04::ScratchCard(cards[mi]));
    }
    // Add copied cards backwards at index i to avoid placing cards
    //  in wrong order
    int newCardIndex = 0;
    for(int mi = i + totalMatches, ni = 0; mi > i; mi--, ni++) {
      cards.emplace(cards.begin() + i, cardsToAdd[ni]);
    }
  }
  return to_string(cards.size());
}
} // namespace solutions
