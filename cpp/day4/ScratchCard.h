//
// Created by Jamie Briggs on 10/12/2023.
// Copyright (c) 2023 jbriggs.dev All rights reserved.
//

#ifndef SCRATCHCARD_H
#define SCRATCHCARD_H

#include "../helper/SolutionInput.h"


namespace day04 {

class ScratchCard {

  int cardNumber;
  vector<int> winingNumbers;
  vector<int> cardNumbers;
public:
  ScratchCard(const std::string &input);
  ScratchCard(ScratchCard* card);
  int pointsWorth();
  int totalMatches();

};

} // day04

#endif //SCRATCHCARD_H
