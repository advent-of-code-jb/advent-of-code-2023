//
// Created by Jamie Briggs on 05/12/2023.
// Copyright (c) 2023 jbriggs.dev All rights reserved.
//

#ifndef DAY_02_H
#define DAY_02_H
#include "../Day.h"
#include "../helper/SolutionInput.h"

using namespace helper;

namespace day2 {
  struct Cube {
    const string type;
    const int total;
  };

  struct Game {
    const int gameNumber;
    const vector<vector<Cube>> sets;
    int maximumRedUsed = 0, maximumGreenUsed = 0, maximumBlueUsed = 0;
    bool gamePlayed = false;

    [[nodiscard]] int totalPowerOfGame() const {
      return maximumRedUsed * maximumGreenUsed * maximumBlueUsed;
    }

    void playGame() {
      for (const auto& set: sets) {
        for (const auto& cubeTaken: set) {
          if (cubeTaken.type == "red" && maximumRedUsed < cubeTaken.total) {
            maximumRedUsed = cubeTaken.total;
          } else if (cubeTaken.type == "green" && maximumGreenUsed < cubeTaken.total) {
            maximumGreenUsed = cubeTaken.total;
          } else if (cubeTaken.type == "blue" && maximumBlueUsed < cubeTaken.total) {
            maximumBlueUsed = cubeTaken.total;
          }
        }
      }
      gamePlayed = true;
    }

    bool isPossibleWhenCubesReplaced(const int totalRedAvailable, const int totalGreenAvailable,
                                     const int totalBlueAvailable) {
      if(!gamePlayed) playGame();
      return !(maximumRedUsed > totalRedAvailable || maximumGreenUsed > totalGreenAvailable || maximumBlueUsed >
          totalBlueAvailable);
    }
  };
}

namespace solutions {
  class Day02 final : public Day {
    static vector<vector<day2::Cube>> getSets(const vector<string>& data);
    static void setupGames(const SolutionInput* input, vector<day2::Game>& games);

    std::string solvePartOne(const helper::SolutionInput* input) override;
    std::string solvePartTwo(const helper::SolutionInput* input) override;
  };
} // solutions

#endif //DAY_02_H
