//
// Created by Jamie Briggs on 05/12/2023.
// Copyright (c) 2023 jbriggs.dev All rights reserved.
//

#include "Day02.h"

#include <iostream>
#include <ostream>

#include "../Day.h"
#include "../helper/NumberUtils.h"
#include "../helper/StringUtils.h"

using namespace helper;

namespace solutions {
  vector<vector<day2::Cube>> Day02::getSets(const vector<string>& input) {
    vector<vector<day2::Cube>> sets;
    for (const auto& i: input) {
      vector<string> setsRaw = StringUtils::splitString(i, ", ");
      vector<day2::Cube> individualSet;
      for (const auto& set: setsRaw) {
        vector<string> setRaw = StringUtils::splitString(set, " ");
        day2::Cube cube = {
          setRaw[1],
        StringUtils::toInt(setRaw[0]),
        };
        individualSet.push_back(cube);
      }
      sets.push_back(individualSet);
    }
    return sets;
  }

  void Day02::setupGames(const SolutionInput* input, vector<day2::Game>& games) {
    for (int x = 0; x < input->getTotalRowsInInput(); x++) {
      const auto gameStringRaw = input->getTestInput()[x];
      vector<string> data = StringUtils::splitString(gameStringRaw, ": ");
      string gameNumber = StringUtils::splitString(data[0], " ")[1];
      vector<string> setsRaw = StringUtils::splitString(data[1], "; ");
      const vector<vector<day2::Cube>> sets = getSets(setsRaw);

      day2::Game game = {
        StringUtils::toInt(gameNumber),
        sets
      };
      games.push_back(game);
    }
  }

  std::string Day02::solvePartOne(const SolutionInput* input) {
    vector<day2::Game> games;
    setupGames(input, games);

    vector<int> possibleGames;
    for(auto & game : games) {
      game.playGame();
      if(game.isPossibleWhenCubesReplaced(12,13,14)) {
        possibleGames.push_back(game.gameNumber);
      }
    }
    return to_string(NumberUtils::totalSum(possibleGames));
  }

  std::string Day02::solvePartTwo(const SolutionInput* input) {
    vector<day2::Game> games;
    setupGames(input, games);

    vector<int> possibleGames;
    for(auto & game : games) {
      game.playGame();
      possibleGames.push_back(game.totalPowerOfGame());

    }
    return to_string(NumberUtils::totalSum(possibleGames));
  }
} // solutions
