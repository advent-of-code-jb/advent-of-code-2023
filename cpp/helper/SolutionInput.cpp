//
// Created by Jamie Briggs on 05/12/2023.
// Copyright (c) 2023 jbriggs.dev All rights reserved.
//

#include "SolutionInput.h"

using namespace helper;

vector<string> SolutionInput::getTestInput() const {
    return this->testInput;
}

