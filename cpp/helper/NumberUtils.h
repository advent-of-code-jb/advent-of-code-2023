//
// Created by Jamie Briggs on 05/12/2023.
// Copyright (c) 2023 jbriggs.dev All rights reserved.
//

#ifndef NUMBERUTILS_H
#define NUMBERUTILS_H
#include <vector>


namespace helper {

class NumberUtils {
public:
  static int totalSum(std::vector<int> input);
};

}


#endif //NUMBERUTILS_H
