//
// Created by Jamie Briggs on 07/12/2023.
// Copyright (c) 2023 jbriggs.dev All rights reserved.
//

#ifndef VECTOR2D_H
#define VECTOR2D_H

namespace core {
  struct Vector2D {
    int x;
    int y;
  };
}

#endif //VECTOR2D_H
